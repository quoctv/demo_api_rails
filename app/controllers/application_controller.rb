class ApplicationController < ActionController::API
  def json_success data
    render  json: { status: 200, data: data , success: true }
  end

  def json_error message
    render  json: { status: 200, success: false, message: message }
  end
end
